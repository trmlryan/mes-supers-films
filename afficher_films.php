<?php
session_start();

require_once "./src/function/Database.php";

require_once "src/models/Film.php";
require_once "src/controller/FilmController.php";

$filmController = new FilmController();

$films = $filmController->getFilms();


include 'header.inc.php';
?>


    <div class="container">
        <div class="card-deck">
            <?php
            foreach ($films as $film) {
                echo '
                    <div class="card">
                        <div class="card-header">' . $film->getTitle() . '</div>
                        <div class="card-body">
                            <img src="' . $film->getJaquette() . '" width="200px">
                            <p>' . $film->getExcerp(200) . '</p>
                        </div>
                        <div class="card-footer">
                            <a href="show_film.php?id=' . $film->getId() . '">
                                <button class="btn btn-primary">Voir +</button>
                            </a>
                        </div>
                    </div>';
            }
            ?>



        </div>
    </div>

    <?php 

    include 'footer.inc.php';